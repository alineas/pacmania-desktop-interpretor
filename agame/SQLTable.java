package agame;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException; 
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import kq7.Main;
 

public class SQLTable {
	private static Map <String,SQLTable> tables = new HashMap<>(); 
	private List<SQLLine> lines = new ArrayList<>();  
	//##################################################
	public SQLTable(String name) {try { 
		 
		
		PreparedStatement ps = Main.sql.prepareStatement("Select * from "+name);
		ResultSet rs = ps.executeQuery();
		while(rs.next())
			try {
				lines.add(new SQLLine(name,Integer.parseInt(rs.getString("id"))));
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
		rs.close();
		ps.close();
	}catch (SQLException e){ e.printStackTrace(); } }
	//##################################################
	public SQLLine getById(int id) {
		for(SQLLine line : lines)
			if(line.getId() == id)
				return line;
		return null;
	}
	//##################################################
	public static SQLTable getTable(String tableName) { tableName=tableName.replaceAll("'", "`");
		if(!tables.containsKey(tableName))
			tables.put(tableName, new SQLTable(tableName));
		return tables.get(tableName);
	} 
	//##################################################
	public List<SQLLine> getLines() { 
		return lines; }  
	static void reload() { 
		for(SQLTable a : tables.values()) 
			a.getLines().clear();
		tables.clear(); 
		
	}  
}