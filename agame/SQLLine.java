package agame;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import kq7.Main;
   

public class SQLLine {
	
	private String table;
	private int id;
	private Map <String,String> datas = new HashMap<>();
	private List <String> columns = new ArrayList<>(); 
	private String cnames ="";  
	private Statement statement;
	
	//##########################################################################################
	public SQLLine(String table, int id) {
		 
		this.table = table .replaceAll("'", "`") ;
		this.id = id ;
		reload();
	}
	//##########################################################################################
	private void reload() {
		columns = getColumns(table);
		for(String a : columns)
			cnames=cnames+a+",";
		cnames=cnames.replaceAll(",$", "");
		datas = getMap(table,id,columns); 
	}
	//##########################################################################################
	public String getString(String column) {
		String d = datas.get(column);
		if(d==null)return"";
		return d.replaceAll("`","'"); }
	//##########################################################################################
	public int getInt(String column) {  
		try {
			return Integer.parseInt(datas.get(column));
		} catch (NumberFormatException e) {
			return -998; //dont touch !!! i used this value cause there is no null check on an integer
		}   
	} 
	//##########################################################################################
	public int getId() {  return id; } 
	public Map<String, String> getDatas() { return datas; }
	public List<String> getColumns() { return columns; }
	//##########################################################################################
	public String[] getArray(String column) {  
		String res = getString(column);
		 res=res.replaceAll(",", ";");
		if(!res.contains(";"))
			return new String[] {res};
		return res.replaceAll("^;", "").split(";");
	} 
	
	//########################################################################################## 
	public LinkedList<Integer> getList(String column) {
		LinkedList<Integer> temp = new LinkedList<>();
		for(String a : getArray(column))
			if(a!=null&&!a.equals(""))
				temp.add(Integer.parseInt(a));
		return temp;
	}
	//#########################################################################
	 List<String> getColumns(String tableName) { try { 
		List<String> columns = new ArrayList<>();
		String sql = "select * from " + tableName + " LIMIT 0";
		if ( statement == null ) statement = Main.sql.createStatement();
		ResultSet rs = statement.executeQuery(sql);
		ResultSetMetaData mrs = rs.getMetaData();
		for(int i = 1; i <= mrs.getColumnCount(); i++)  
			columns.add(mrs.getColumnLabel(i)); 
		rs.close();
		 
		return columns;
	} catch (Exception e1) { e1.printStackTrace(); }return null;}
	 //##################################################
	 Map<String, String> getMap(String table, int id, List<String> columns) {try {  
		 Map<String,String> temp = new HashMap<>();
		 PreparedStatement ps = Main.sql.prepareStatement("Select * from "+table+" where id = "+id);
		 ResultSet rs = ps.executeQuery();
		 while (rs.next())
			for(String col : columns)
				temp.put(col, rs.getString(col));
		 rs.close();ps.close();
         return temp;
	 }catch (SQLException e){ e.printStackTrace(); }return null;}
 
	 
	 public Map<String,String> duoArray(String column) {
	        String[] array = getArray(column);
	        Map<String,String> temp = new HashMap<>();  
			for(String l : array) { if(l.length()<2)continue;// empty "^;" 
				 temp.put( l.split("=")[0].replaceAll(" ", ""),l.split("=")[1].replaceAll(" ", "") );
			}
			return temp; 
	    }
	 public int getInt(String column, String key) {  
		 try {
			return Integer.parseInt(duoArray(column).get(key));
		} catch (NumberFormatException e) {
			 Utils.log( key+" was not found in column "+column);
		}
		return -998;
	            
	    } 
		public String toStr() {String s = "";
		for(Entry<String, String> a : datas.entrySet())
			s=s+" --- "+a.getKey()+":"+a.getValue();
		return s;
	}
	 public String getString(String column, String key) { 
		 return  duoArray(column).get(key) ;
	    } 
}
