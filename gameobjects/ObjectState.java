package gameobjects;

import java.util.Collection; 
import java.util.Set; 
import agame.SQLLine; 
import agame.Utils.Position;
import gameobjects.GameObject;

public class ObjectState extends GameObject{  
	 
	final boolean canBeFullscreen;
	final String cursor;
	final int anim;
	final int translate;
	private Position pos;
	private Position reqPos;
	
	private ObjectState(SQLLine line ) {
		super(line);   
		canBeFullscreen = line.getInt("fullscreen")==1; 
		String s = line.getString("cursorimage");
		if( s==null && this.animID()!=-998) 
			s=Animation.get(this.animID()).image();
		cursor = s;
		anim = line.getInt("animtransitionid");
		translate = line.getInt("translateid");
	}  
	public ObjectState() { canBeFullscreen=false;cursor=null;anim=translate=-1;}//fake
	//########################################################################
  
	public Position pos() {   if(pos==null)pos=new Position(line.getString("pos"));return pos;  } 
 	public Position requiredPlayerPosition() { if (reqPos==null)reqPos= new Position(line.getString("requiredplayerposition")); return reqPos;   }  
  	public int animID() { return anim; }  
  	public boolean isFullscreen() { return canBeFullscreen; }  
 	public String cursorImg() { return cursor;} 
 	public int translateId() { return translate ; }  
	@Override public String getFolder() {  return possessorObj().getFolder(); } 
 
	@Override GameObject instanciate(SQLLine l) {  return new ObjectState(l); }
	@Override boolean isValidPossessor(SQLLine l, GameObject possessor) { 
		return l.getInt("possessor")==possessor.line.getId() ; }
	@Override boolean isValidElement(SQLLine l) {  return true; }
	@Override String getTable() {  return "objectstate"; }
	static ObjectState instance= new ObjectState();
	@Override GameObject default_instance() { return instance; } 
	public static Set<ObjectState> getByObject(int objectID) {
		return getbyPossessor(instance,GameObject.getById(objectID));  }  
	public static ObjectState get(int id) {  return get(instance,id); }  
	public static Collection<? extends GameObject> getAll() { return getAll(instance); }
  
	@Override public String title() {
		try { return this.possessorObj().title()+"_state_"+super.title();
		} catch (Exception e) {  return super.title(); }
	}
}
