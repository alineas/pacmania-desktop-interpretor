package gameobjects;  
import java.util.HashSet; 
import java.util.Set; 
import agame.SQLLine;
import agame.SQLTable;
import agame.Utils.Position; 

public class TransitionPoint extends Zone  {  
 
	final Position desiredPlacement;  
	final int sourceScene,destinationScene,sourceId,destinationId; 
	 
	//##########################################################
	public TransitionPoint(String sql,int id) { 
		super(sql,id);
		String[] split = sql.split(","); 
		int pointX = Integer.parseInt(split[5]);
		int pointY = Integer.parseInt(split[6]);  
		int sourceSceneId = Integer.parseInt(split[7]);
		int sourcePointId = Integer.parseInt(split[8]); 
		int destinationSceneId = Integer.parseInt(split[9]);
		int destinationPointId = Integer.parseInt(split[10]); 
		int dir = Integer.parseInt(split[11]); 
  
		destinationScene=destinationSceneId;
		sourceId=sourcePointId;
		destinationId=destinationPointId; 
		desiredPlacement = new Position(pointX,pointY,dir);
		sourceScene = sourceSceneId; 
		  
	}
	//##########################################################
	@Override GameObject default_instance() {  return instance; }//fake
	static TransitionPoint instance = new TransitionPoint( );
	public TransitionPoint() { sourceScene=destinationScene=destinationId=sourceId=-1;desiredPlacement=null; }
	//##########################################################
	public int getDestinationScene() { return destinationScene; } 
	public int getSourceId() { return sourceId; } 
	public int getDestinationId() { return destinationId; }  
	public int getSourceScene() { return sourceScene; }
	public Position getDesiredPos() {return desiredPlacement;}  
	  
	//########################################################## 
	@Override public int hashCode() { final int prime = 31;
		int result = 1; result = prime * result + sourceId; return result; }
	@Override public boolean equals(Object obj) {
		if (this == obj) return true; if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		TransitionPoint other = (TransitionPoint) obj;
		if (sourceId != other.sourceId) return false;
		return true;
	}  
	 
	@Override
	boolean isValidPossessor(SQLLine l, GameObject owner) { 
		return l.getInt("possessor") == owner.line.getId()   ;
	}

	@Override
	boolean isValidElement(SQLLine l) { 
		 if (l.getInt("type") == ZoneSort.TPOINT.getValeur())
     		return true;
		return false;
	}

	//########################################################## 
	public static Set<Zone> getByOwner(int ownerId){
		return getbyPossessor(instance,GameObject.getById(ownerId));  
		 
	}
	
	public static Set<Zone> getAll( ) {
		return getAll(instance);
	}
	//########################################################## 
	public static Zone get( int id) { return get(instance,id); }


}
 