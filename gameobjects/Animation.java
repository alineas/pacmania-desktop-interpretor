package gameobjects;
 
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Set;

import kq7.Img;
import kq7.ImgUtils;
import kq7.Main;

import agame.SQLLine;
import agame.SQLTable;
import agame.Utils;
import agame.Utils.Position; 

public class Animation extends GameObject   {  
	//###############################################
	private int y, x, index = 0;  public boolean idle, reversed; 
	//###############################################
	private final Position posFrom,posTo ;  
	private final int shad,prioTo,priority,priorityToRem,prioFrom ;
	private final LinkedList<Integer> frameOrder,sonorizedFrames; 
	private   AnimationState state;
	private final String sound,image,music,folder; 
	//###############################################
	static Animation instance = new Animation();//fake
	public Animation() { posFrom=posTo=null;sound=image=music=folder=null;state=null;
		shad=prioTo=priority=priorityToRem=prioFrom=-1;frameOrder=sonorizedFrames=null;}
	//###############################################
	public Animation(SQLLine line) {
		super(line); int temp = -1;  
		frameOrder = line.getList("frameorder");
		sonorizedFrames = line.getList("sonorizedframes");
		posFrom = new Position(line.getString("posfrom"));
		posTo= new Position(line.getString("posto"));
		music = line.getString("music");
		temp=  line.getInt("priofrom") ; if(temp==-998)prioFrom=0;else prioFrom=temp;
		temp=  line.getInt("prioto") ;if(temp==-998)prioTo=0;else prioTo=temp;
		temp=line.getInt("shadow");if(temp==-998)shad=-1;else shad=temp;
		try {
			state = AnimationState.from(line.getInt("state"));
		} catch (Exception e1) {
			Main.log("erreur anim "+title());
		}
		sound = line.getString("sound");
		image = line.getString("img");
		
		//bad namings...
		temp = line.getInt("takepriority");if(temp == -998) priority=0;else priority=temp;// inc y position
		temp = line.getInt("priority");if(temp == -998) priorityToRem=0;else priorityToRem=temp;//specify position y for prioritized animations
		
		String fol = "";
		if(!sound.equals("")||!music.equals(""))
			try {fol = possessorObj().possessorObj().getFolder();
				 } catch (Exception e) {Main.log("erreur sounde "+sound+" "+possessorObj().title() );} 
			
		folder=fol; 
				  
	}
	//###############################################
	public static Animation get(int id) { return get(instance,id); }  
	public static Set<GameObject> getAll() { return getAll(instance); }  
	public static Set<Animation> getAnimations(GameObject el) {
		 
		return getbyPossessor(instance,el);
	} 
	//###############################################
	public Img loadGif() {//dont load all direct...
		 Img temp = null;
		
		if(!image().contains("/"))
			temp= ImgUtils.getImg(GameObject.getById(possessor()).getFolder()+image(),shadow()); 
		else 
			temp= ImgUtils.getImg( image(),shadow()); 
		
	    if(state()==AnimationState.MONO)
	    	setIdleFrame( 0); 
	    return temp;
	}
  
	//######################################################################## 
 
	public void paintIt(Graphics g ) {  
	    loadGif(); 
	    if (state() != AnimationState.INVISIBLE) { try {
	        	if (state() != AnimationState.MONO){
	                setIndex(getIndex() + 1);
	                if( sonorizedFrames().contains(getIndex())) 
 			                Main .instance.soundPlayer.playAudio(folder+sounde(),true);
	                
	            }
	            if (getIndex() >= this.frameOrder().size()) {
	                if (state() == AnimationState.INFINITE_LOOP || 
	                		state() == AnimationState.ALEATORY_LOOP && new Random().nextBoolean()
	                		&& new Random().nextBoolean()&& new Random().nextBoolean()
	                		&& new Random().nextBoolean()) {//TODO : make better
	                    setIndex(0); 
	                    if( sonorizedFrames().contains(getIndex())){
 	 		                Main .instance.soundPlayer.playAudio(folder+sounde(),true);
	                    }
 	                }else
	                    setIndex(getIndex() - 1);   
	            }
	               
	            BufferedImage movingImage = ImgUtils.getCopy(this.getActualFrame()) ; 
	            if(priorityToremove()!=0||priorityToAddForBypass()!=0) {try { 
	            	final int addx = (int) (x -  posFrom().x );//used only if specified for ground obj
					final int addy = (int) (y -  posFrom().y ); 
					
	    			movingImage = Utils.unprioritize(movingImage,addx,addy,y+movingImage.getHeight()+priorityToAddForBypass(),false,null); 
	    			movingImage = ImgUtils.scal (movingImage,Main.zoom);
	    			g.drawImage( movingImage , (int) (x*Main.zoom), (int) (y*Main.zoom), null); 
	    		} catch (Exception e) { e.printStackTrace(); }}
	             
	            else {  
	            	movingImage= ImgUtils.scal (movingImage,Main.zoom); 
	             	g.drawImage(movingImage, (int) (x*Main.zoom), (int) (y*Main.zoom), null);
	            }
	        } catch (Exception e) {e.printStackTrace(); } }
	}

 
	
	//############################################### 
	public void run() {
		 if (!idle) { loadGif();
	    if (state() != AnimationState.INVISIBLE) {  
	        if (state() != AnimationState.MONO) {
	            if (!reversed)
	                setIndex(getIndex() + 1);  
	            else if (getIndex() > 0)
	                setIndex(getIndex() - 1);  
	        }

	        if (getIndex() >= frameOrder().size()) {
	            if (state() == AnimationState.INFINITE_LOOP)
	                setIndex(0);  
	            else if (getIndex() > 0)
	                setIndex(getIndex() - 1); 
	        }else{
	        	 if( sonorizedFrames().contains(getIndex())){
 		                Main .instance.soundPlayer.playAudio(folder+sounde(),true);
	        	 }
	        }
	    }
	}}

	//###############################################
	public void rewindAll() { setIndex(0); 
		if(!sounde().equals("")&& sonorizedFrames().size()==0)
			Main .instance.soundPlayer.playAudio(folder+sounde(),true); 
		if(!music().equals("") && !Main.instance.soundPlayer.actualMidiPath.equals(music()))
			Main .instance.soundPlayer.playMidi(folder+music() ,true,false);
	
	}
	//###############################################
	public boolean loopFinished() {  
	    return state()==AnimationState.INVISIBLE
	    		||state()==AnimationState.MONO
	    		||!reversed&&getIndex() >= frameOrder().size()-1
	    		|| reversed&&getIndex() == 0; }
  
	//###############################################   
	@Override public String getFolder() { return GameObject.getById(possessor()).getFolder(); }
	@Override GameObject instanciate(SQLLine l) { return new Animation(l); } 
	@Override boolean isValidElement(SQLLine l) {  return true; } 
	@Override String getTable() {  return "animation"; } 
	@Override GameObject default_instance() { return instance; } 
	@Override boolean isValidPossessor(SQLLine l, GameObject possessor) { 
		return possessor.line.getId() == l.getInt("possessor") ;  }   
	//############################################### 
	public int priorityToAddForBypass() { return priority; } 
	public int priorityToremove() { return priorityToRem; }
	public List<Integer> frameOrder() {  return frameOrder; }  
	public LinkedList<Integer> sonorizedFrames(){ return sonorizedFrames; }  
	public Position posFrom() {  return posFrom; } 
	public Position posTo() { return posTo; } 
	public String music() { return music; }   
	public int prioFrom() {  return prioFrom; } 
	public int prioTo() {   return prioTo; } 
	public String image() { return image; } 
	public String sounde() { return sound; }   
	public int shadow() { return shad; }   
	public AnimationState state( ) { return state; }    
	public List<BufferedImage> getFrames() {  return loadGif().frames; } //editor
	public BufferedImage getActualFrame() { return getFrames().get(this.frameOrder().get(getIndex())); }
	public BufferedImage getFirstFrame() { return getFrames().get(0); }
	public BufferedImage getLastFrame() { return getFrames().get(frameOrder().get(frameOrder().size()-1)); }
	public void setIdleFrame(int idleFrame) { setIndex(idleFrame); idle=true; }
	public int currentIndex() { return getIndex(); }
	public void rewindOneFrame() {setIndex(getIndex() > 0 ? getIndex() - 1 : frameOrder().size() - 1);} 
	public void forwardOneFrame() { setIndex(getIndex() < frameOrder().size() - 1 ? getIndex() + 1 : 0);} 
	public void forwardAll() { setIndex(frameOrder().size()-1); } 
	public void setReversed(boolean b) { reversed = b; } 
	public boolean isReversed() {  return reversed; } 
	public void unsetIdle() { idle=false; } 
	public int getIndex() {  return index; } 
	public void setIndex(int newIndex) {  index = newIndex;  }
	public int getX() { return x; } 
	public int getY() { return y; } 
	public int lastFrameNumber() { return frameOrder().get(frameOrder().size()-1); }
	public int getHeight() { return getActualFrame().getHeight(); }   
	public void setLocation(int x2, int y2) { x=x2;y=y2; }
	   
	public static  enum AnimationState {
		INVISIBLE(-1),INFINITE_LOOP(-2),LOOP(-3), MONO(-4),ALEATORY_LOOP(-5); 
		private final int valeur; 
		AnimationState(int v) { valeur = v;  } 
		public int getValeur() {  return valeur; } 
		public static AnimationState from(String s) { 
			return from(Integer.parseInt(s)); }
		public static AnimationState from(int valeur) {
			for (AnimationState type : values())  if (type.getValeur() == valeur)  return type;  
			throw new IllegalArgumentException("no animation for " + valeur );
		}
	}
}
