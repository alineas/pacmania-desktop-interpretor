package gameobjects;
 
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet; 
import java.util.Map;
import java.util.Set;

import kq7.Main; 
import agame.SQLLine;
import agame.SQLTable; 
import gameobjects.GameObject;
 
public abstract class GameObject  { 
 
	final static Map<GameObject, Map<Integer,GameObject>> mappe = new HashMap<>();//all the data!
	public final SQLLine line; 
	public final int id;
	public final int possessor;
	public final String img;
	
	//###############################################
	public GameObject(SQLLine line){   
		this.line = line; 
		this.id=line.getId(); 
		possessor = line.getInt("possessor");
		img = line.getString("img");
	}  
	//###############################################
	public GameObject() { possessor=id=-1;line=null;img=null;}//fake
	//###############################################
	public static <T extends GameObject> T get( GameObject  instance, int id) {  
		
		if (!mappe.containsKey(instance))  
            mappe.put(instance, new HashMap<Integer,GameObject>()); 
		 
		Map<Integer,GameObject> sparseArray = mappe.get(instance); 
		
        if (!sparseArray.containsKey(id) ) { 
			for (SQLLine l : SQLTable.getTable(instance.getTable()).getLines()) 
				if(l.getId() == id && instance.isValidElement(l))
			        mappe.get(instance).put(id, instance.instanciate(l));
		}
		
        return (T) mappe.get(instance).get(id); 
    }
	//###############################################
	public static  <T extends GameObject> Set<T> getAll(GameObject clazz) { 
		Set<GameObject> allGameObjects = new HashSet<GameObject>(); 
		String tableName = clazz.getTable();
		Main.log("loading "+tableName);
		for (SQLLine l : SQLTable.getTable(tableName).getLines()) 
			if(clazz.isValidElement(l))
				allGameObjects.add(get(clazz, l.getId()));
		return (Set<T>) allGameObjects;
	}
	//###############################################
	public static  <T extends GameObject> Set<T> getbyPossessor(GameObject clazz, GameObject possessor ) {  
		Set<GameObject> allGameObjects = new HashSet<GameObject>();  
		for (SQLLine l : SQLTable.getTable(clazz.getTable()).getLines()) 
			if(clazz.isValidPossessor(l, possessor)  && clazz.isValidElement(l))
				allGameObjects.add(get(clazz, l.getId()));
		return (Set<T>) allGameObjects;
	}
	//###############################################
	public static GameObject getById(int int1) {
		if(Scene.get(int1) != null)return Scene.get(int1); 
		if(CharacterState.get(int1)!= null)return CharacterState.get(int1);
		if(InventoryObject.get(int1)!= null)return InventoryObject.get(int1);
		if(ObjectState.get(int1)!= null)return ObjectState.get(int1);
		if(Character.get(int1) != null)return Character.get(int1); 
		if(BasicObject.get(int1) != null)return BasicObject.get(int1); 
		if(Message.get(int1) != null)return Message.get(int1); 
		if(Chapter.get(int1) != null)return Chapter.get(int1); 
		if(Action.get(int1) != null)return Action.get(int1); 
		if(Zone.get(int1) != null)return Zone.get(int1); 
		if(TransitionPoint.get(int1) != null)return TransitionPoint.get(int1); 
		if(Stair.get(int1) != null)return Stair.get(int1); 
		if(StairList.get(int1) != null)return StairList.get(int1); 
		if(SpawnPoint.get(int1) != null)return SpawnPoint.get(int1); 
		if(SpecialZoneInv.get(int1) != null)return SpecialZoneInv.get(int1); 
		return null;
	}
	//###############################################
	public String title(){
		String s = line.getString("title");
		if(s.equals(""))s="no title defined";
		return s;}
	//###############################################
    abstract GameObject instanciate(SQLLine l); 
	abstract boolean isValidPossessor(SQLLine l, GameObject possessor);
	abstract boolean isValidElement(SQLLine l);
    abstract String getTable( );
    abstract GameObject default_instance();
    public int possessor() { return possessor ; }  
    public GameObject possessorObj() { return getById(possessor); }   
	abstract public String getFolder();  
	public String image() { return img; }  
	@Override public boolean equals(Object obj) { if(!(obj instanceof GameObject))return false;
	GameObject other = (GameObject) obj; if (id != other.id) return false; return true; }  
	//###############################################
	public static void reset() {
		for( Map<Integer, GameObject> a:mappe.values())
			a.clear();
		mappe.clear(); 
	}
}
