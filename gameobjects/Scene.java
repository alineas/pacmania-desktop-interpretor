package gameobjects;
 
import java.awt.image.BufferedImage;
import java.util.Map;
import java.util.Set;

import kq7.Img;
import kq7.ImgUtils;
import kq7.Main;

import agame.SQLLine;
import agame.Utils;
import gameobjects.GameObject;

public class Scene extends GameObject {  
	final int scalefactorbotlimit,scalefactortoplimit,scalepercents ,loop;
	final Map<String, String> zindexes; BufferedImage bgImage;
	BufferedImage priBgImage;
	final String bgName,priBgFileName,music;
	private boolean replaceSong;
	
 	public Scene(SQLLine line) { 
 		super(line); 
 		scalefactorbotlimit=line.getInt("scalefactor", "scalefactorbotlimit");
 		scalefactortoplimit=line.getInt("scalefactor", "scalefactortoplimit");
 		scalepercents=line.getInt("scalefactor", "scalepercents");
 		zindexes=line.duoArray("zindexes"); 
 		bgName = line.getString("img");
 		if(! bgName().contains("/")) 
			bgImage= ImgUtils.getPicture(getFolder()+bgName() );
		else bgImage= ImgUtils.getPicture( bgName() );//copieds scenes call orininal image path
 		priBgFileName = line .getString("img2"); 
 		
		BufferedImage priBg=null;
		try {//there is no pri bg in all scenes
			if(! priBgFileName.contains("/")) priBg = ImgUtils.getPicture(getFolder()+priBgFileName) ;
			else priBg= ImgUtils.getPicture( priBgFileName) ;
		} catch (Exception e) { }
		priBgImage = priBg;
		loop=line .getInt("loopmusic") ;
		music = line .getString("music");
		replaceSong = line .getString("replacesong").equals("true");
 	} 
	public Scene( ) {scalefactorbotlimit=scalefactortoplimit=loop=scalepercents=-1;zindexes=null;bgName=priBgFileName=music=null; }
	
	
	public int scalefactorBotLimit() {return scalefactorbotlimit;}
	public int scalefactorTopLimit() {return scalefactortoplimit;}
	public int scalePercents() {return scalepercents;}
	public Map<String, String> zindexes(){ return zindexes;} 
	public BufferedImage bgImage(){ return bgImage; } 
	public BufferedImage priorityBgImage(){ return priBgImage;} 
	public String bgName() { return bgName; } 
	public String priorityBg() {  return priBgFileName; }  
	public int getZindex(int rgb) { return Integer.parseInt(zindexes.get(rgb+"")); } 
	@Override public String getFolder() { return "games/"+Main.gameName+"/scenes/"+title()+"/";  }
	public String music() {  return music; } 
  
	@Override GameObject instanciate(SQLLine l) {  return new Scene(l); } 
	@Override boolean isValidPossessor(SQLLine l, GameObject possessor) {  return false; } 
	@Override boolean isValidElement(SQLLine l) { return true; } 
	@Override String getTable() { return "scene"; }
	static Scene instance= new Scene( );
	@Override GameObject default_instance() {  return instance; } 
	public static Scene get(int id) { return get(instance,id); }  
	public static Set<Scene> getScenes() { return getAll(instance); }
	public boolean replaceSong() { return replaceSong ; } 
	public int loopMusic() {  return loop; } 
}
