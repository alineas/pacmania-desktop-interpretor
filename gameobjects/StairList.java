package gameobjects;
 
import java.util.Set;

import agame.SQLLine;

public class StairList extends Zone {
 
	public StairList(String sql, int id) { super(sql,id); } 
	public StairList() { }//bad naming, its one of the zones who are inside one stair
	//###############################################
	@Override boolean isValidPossessor(SQLLine l, GameObject owner) { 
		return l.getInt("type") == ZoneSort.STAIRLIST.getValeur() && l.getInt("possessor") == owner.line.getId();
	}

	@Override boolean isValidElement(SQLLine l) { 
		 if (l.getInt("type") == ZoneSort.STAIRLIST.getValeur())
     		return true;
		return false;
	}
	//###############################################
	public static Set<Zone> getByOwner(int ownerId){
		 return getbyPossessor(instance,GameObject.getById(ownerId));
	}
	//###############################################
	static StairList instance = new StairList( );
	@Override GameObject default_instance() {  return instance; }
	public static Set<Zone> getAll( ) { return getAll(instance); }
	public static Zone get( int id) { return get(instance,id); }
}
