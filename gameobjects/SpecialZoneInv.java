package gameobjects;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import agame.SQLLine;
   
public class SpecialZoneInv extends Zone{ 

	public List<Integer> frames=new ArrayList<Integer>(); //frame list in inv object explorer
	public SpecialZoneInv(String sql, int id) {
		super(sql,id);
		
		String[] frams = sql.split(",")[5].split("@");
		for(String s : frams)
			frames.add(Integer.parseInt(s));
		if(!sql.split(",")[5].contains("@") && sql.split(",")[5].length()!=0)
			frames.add(Integer.parseInt(sql.split(",")[5]));
	} 
	 
	public SpecialZoneInv() { }
 
	@Override boolean isValidPossessor(SQLLine l, GameObject owner) { 
		return  l.getInt("possessor") == owner.line.getId()  ;
	}

	@Override boolean isValidElement(SQLLine l) { 
		 if (l.getInt("type") == ZoneSort.SPECIAL.getValeur())
     		return true;
		return false;
	}

	static SpecialZoneInv instance = new SpecialZoneInv( );
	@Override GameObject default_instance() {  return instance; } 
	public static Set<Zone> getByOwner(int ownerId){ 
		 return getbyPossessor(instance,GameObject.getById(ownerId));
	}
  
	public static Set<Zone> getAll( ) {  return getAll(instance); } 
	public static Zone get( int id) { return get(instance,id); }
	 
	@Override public boolean equals(Object obj) {
		if(!(obj instanceof SpecialZoneInv)) return false;
		if (this == obj)
			return true; 
	
		Zone other = (Zone) obj;
		if (p1 == null) {
			if (other.p1 != null)
				return false;
		} else if (!p1.equals(other.p1))
			return false;
		if (p2 == null) {
			if (other.p2 != null)
				return false;
		} else if (!p2.equals(other.p2))
			return false;
		return true;
	}
	 

}
