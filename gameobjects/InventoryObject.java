package gameobjects;
 
import java.util.Set;

import kq7.Main; 

import agame.SQLLine;
import agame.SQLTable; 

public class InventoryObject extends BasicObject { 
	public Zone zone;//position in menu
	 
	public InventoryObject() { }
	public InventoryObject(SQLLine l) { super(l); }
	//###############################################
	public void setSelectedState(ObjectState selectedState) { this.selectedState = selectedState; }  
	public ObjectState initialState() {return ObjectState.get(line.getInt("initialstate"));}
	@Override public String getFolder() {
		if(type()!= ObjectType.INVENTORY )return null;
		String path = "games/"+Main.gameName+"/invobjects/";
		return  path ; 
	}
	//###############################################
	public static Set<BasicObject> getAll() {return getAll(instance); } 
	  
	@Override boolean isValidElement(SQLLine l) { 
		return l.getInt("type") == ObjectType.INVENTORY.getValeur();
	}
	@Override boolean isValidPossessor(SQLLine l, GameObject possessor) {
		return false; //inv have no possessor
	}
	static InventoryObject instance  = new InventoryObject();
	 
	@Override GameObject default_instance() { 
		return instance;
	}
	@Override GameObject instanciate(SQLLine l) {
		if( l.getInt("type") == ObjectType.INVENTORY.getValeur())  
			return new InventoryObject(l);  
		return new BasicObject(l);
		
	}
	public static BasicObject get(int id) {return get(instance,id);}
	static SQLTable table = SQLTable.getTable("basicobject");  
	public void setZone(Zone zone2) { this.zone=zone2;  }

	 
	
}
