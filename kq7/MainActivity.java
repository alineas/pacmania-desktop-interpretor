package kq7;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import agame.Utils;
import javafx.application.Application;
import java.io.IOException;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.scene.media.Media; 
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.stage.Stage;

public class MainActivity extends Application { 
	
	MediaPlayer mediaPlayer;
	
	public static void main(String[] args) { launch(args); }
	
	@Override public void start(Stage primaryStage) {
	    primaryStage.setTitle("King Quest 7 Intro"); 
	    MediaView mediaView = new MediaView(); 
	    InputStream str = Thread.currentThread().getContextClassLoader().getResourceAsStream("intro.mp4");
	    File videoFile = createTemp(str);
	    if (videoFile.exists()) { playVideo(videoFile, primaryStage, mediaView);
	    } else { Utils.log("mp4 vid not found");  new Main(); } 
	    StackPane root = new StackPane(mediaView);
	    Scene scene = new Scene(root, 800, 600);
	    mediaView.fitWidthProperty().bind(scene.widthProperty());
        mediaView.fitHeightProperty().bind(scene.heightProperty()); 
        mediaView.setOnMouseClicked(event -> {
            mediaPlayer.stop();  primaryStage.close();   new Main();   });
	    primaryStage.setScene(scene);
	    primaryStage.show();
	}
	
	
    private File createTemp(InputStream inputStream){
        File tempFile=null;try {
        tempFile = File.createTempFile("temp-video", ".mp4"); 
        tempFile.deleteOnExit();  
        try (FileOutputStream outputStream = new FileOutputStream(tempFile)) {
            byte[] buffer = new byte[4096];
            int bytesRead;
            while ((bytesRead = inputStream.read(buffer)) != -1)  
                outputStream.write(buffer, 0, bytesRead); 
        }} catch (IOException e) { e.printStackTrace();}
        return tempFile;
    }
    
    
	private void playVideo(File file, Stage stage, MediaView mediaView) {
	    String filePath = file.toURI().toString();
	    Media media = new Media(filePath);
	    mediaPlayer = new MediaPlayer(media);
	    mediaPlayer.setAutoPlay(true);
	    mediaView.setMediaPlayer(mediaPlayer); 
	    mediaPlayer.setOnError(() -> { Utils.log("vid error " + mediaPlayer.getError().getMessage());  }); 
	    mediaPlayer.setOnEndOfMedia(() -> {new Main(); });
	}

   
}
