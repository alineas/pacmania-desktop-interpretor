package kq7;
  
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import agame.SoundPlayer;
import agame.Utils;
import agame.Utils.Point;  
import gameobjects.Zone;

public class IgMenu extends JPanel {//TODO : all the menu
	private JSlider seekBarFX; 	private JSlider seekBarMusic; 	private JButton restoreButton;
	private JLabel exitButton;
	private JTextField musicVolumeLabel;
	private JTextField fxVolumeLabel;
	private float zoom =1f;
	private BufferedImage bg; 
	//###################################################  
	 
    public IgMenu( )  {try {  
    	MenuBar.igMenu = this;
    	 bg = ImgUtils.getPicture(igMenuBg());  
    	 Utils.setSize(this, bg.getWidth(), bg.getHeight());
    	setBounds(0,0, bg.getWidth(), bg.getHeight());
    	setLayout(null);
        musicVolumeLabel = new JTextField();  musicVolumeLabel.setText("Music Volume"); musicVolumeLabel.setBackground(Color.WHITE);
        fxVolumeLabel = new JTextField();    fxVolumeLabel.setText("FX Volume");fxVolumeLabel.setBackground(Color.WHITE);
        
        seekBarFX = new JSlider(-20,-1,-1); seekBarFX.setBackground(Color.WHITE);
       
        seekBarMusic = new JSlider(0,128,60); seekBarMusic.setBackground(Color.WHITE);
        
        restoreButton = new JButton ("Restore Game");restoreButton.setBackground(Color.WHITE);
        
        BufferedImage bmp = ImgUtils.getPicture(exitButtonMenuBg());
        exitButton = new JLabel(); exitButton.setIcon(new ImageIcon(bmp));
        
        setBounds(musicVolumeLabel,10,100,    100,20,true);//TODO: need to add these zones in the igmenu editor
        setBounds(seekBarMusic,   120,100,    100,20,true);
        setBounds(fxVolumeLabel,   10,120,    100,20,true);
        setBounds(seekBarFX,      120,120,    100,20,true);
        setBounds(restoreButton,  10,140,    100,40,true);
        setBounds(exitButton,     closeMenuZone().x(),closeMenuZone().y(),    closeMenuZone().w(),closeMenuZone().h(),true);
 
        	 
        seekBarFX.addChangeListener(new ChangeListener() {
            @Override public void stateChanged(ChangeEvent e) { 
                SoundPlayer.fxVolume = seekBarFX.getValue();Utils.log("new sound value : "+seekBarFX.getValue());
            }
        });

        seekBarMusic.addChangeListener(new ChangeListener() {
            @Override public void stateChanged(ChangeEvent e) { 
                SoundPlayer.midiVolume =seekBarMusic.getValue();Utils.log("new sound value : "+seekBarMusic.getValue());
            }
        });

        restoreButton.addActionListener(new ActionListener() {
            @Override public void actionPerformed(ActionEvent e) {
                Main.instance.restoreGame();
            }
        });
       
        goVisibility(false);
        
    } catch (Exception e) { e.printStackTrace();} }
 
    
    @Override protected void paintComponent(Graphics canvas) { 
    	canvas.drawImage(bg, 0, 0, null );
    	
    }
  private void setBounds( Component v, int i, int j, int width, int height,boolean add) { 
  	v.setBounds(i,j,width,height);
  	Utils.setSize(v, width, height);
  	if(add)add(v);
	}
 
  //###################################################  
    public void goVisibility(boolean visible) {
        int componentCount = getComponentCount();
        for (int i = 0; i < componentCount; i++) {
            Component component = getComponent(i);
            if (!visible) {
                component.setVisible(false);
                setVisible(false);
                revalidate();
                repaint();
            } else {
                setVisible(true);
                component.setVisible(true);
                revalidate();
                repaint();
            }
        }
    }
    
    
    public Zone closeMenuZone() { return Zone.get(MenuBar.getGameInfo().getInt("menubar","closemenuzone"));}
    public String igMenuBg(){return "games/"+Main .gameName+"/"+MenuBar.getGameInfo().getString("menubar", "igmenubg") ;}
	public String exitButtonMenuBg(){return "games/"+Main .gameName+"/"+MenuBar.getGameInfo().getString("menubar", "exitmenubg") ;}
	
	public void setZoom(float zoom) {this.zoom=zoom; 
	    bg =  ImgUtils.getPicture(igMenuBg()) ; 
	    int newWidth = (int) (bg.getWidth() * zoom);
	    int newHeight = (int) (bg.getHeight() * zoom);
	    bg = ImgUtils.resiz (bg, newWidth, newHeight);
  Utils.setSize(this, newWidth, newHeight);
 
	    setBounds(musicVolumeLabel, (int) (10 * zoom), (int) (100 * zoom), (int) (100 * zoom), (int) (20 * zoom),false);
	    setBounds(seekBarMusic, (int) (120 * zoom), (int) (100 * zoom), (int) (100 * zoom), (int) (20 * zoom),false);
	    setBounds(fxVolumeLabel, (int) (10 * zoom), (int) (120 * zoom), (int) (100 * zoom), (int) (20 * zoom),false);
	    setBounds(seekBarFX, (int) (120 * zoom), (int) (120 * zoom), (int) (100 * zoom), (int) (20 * zoom),false);
	    setBounds(restoreButton, (int) (10 * zoom), (int) (140 * zoom), (int) (100 * zoom), (int) (40 * zoom),false);
	    setBounds(exitButton, (int) (closeMenuZone().x() * zoom), (int) (closeMenuZone().y() * zoom), 
	               (int) (closeMenuZone().w() * zoom), (int) (closeMenuZone().h() * zoom),false);
	}

	 
 
	public void mouseReleased(int x, int y) {
		if(closeMenuZone().isInZone(new Point((int)( x/zoom),(int) (y/zoom)))) {
    		Main.instance.videoPanel.menuBar.freezed=false;
    		goVisibility(false); 
		}
	}
}
