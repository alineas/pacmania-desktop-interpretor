package kq7;
   
import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.swing.JLabel;
import javax.swing.JPanel;

import agame.Ashe;
import agame.SQLLine;
import agame.SQLTable;
import agame.Utils;
import agame.Ashe.AshType;
import agame.Utils.Point; 
import gameobjects.Action;
import gameobjects.Animation; 
import gameobjects.Zone; 
import gameobjects.BasicObject;
import gameobjects.InventoryObject;
import gameobjects.Zone.ZoneType; 

public class MenuBar extends JPanel { 
	
    public List<InventoryObject> inventory = new ArrayList<>(); 
    public boolean freezed;
    public InventoryObject inHand;
    public Animation inHandAnim; 
	public BasicObject inHandObj; //ground objects we can take 
	private int menuX,menuY,menuWidth,menuHeight; 
	public static InvExplorer invExplorer;
	public static IgMenu igMenu;
	public static int inHandY,inHandX; 
    float zoom = 1f;
	private BufferedImage bg;
	BufferedImage inHandBmp;
	
	//############################################################################
 
	public MenuBar( ) { try { 
		 igMenu=new IgMenu();invExplorer = new InvExplorer(); 
		   bg=ImgUtils.getPicture(bgFolder()); 
		menuX = menuZone().x();  menuY = menuZone().y(); 
		menuWidth = menuZone().w();  menuHeight = menuZone().h();    
		setBounds(menuX ,menuY,menuWidth,menuHeight);
		Utils.setSize(this, menuWidth, menuHeight);
		setBackground(Color.BLACK);
		setVisible(true);
	} catch (Exception e) { e.printStackTrace(); }}
		 
	//############################################################################ paint
    @Override protected void paintComponent(Graphics canvas) { try { 
    	if(igMenu.isVisible())igMenu.repaint();
    	if(invExplorer.isVisible())invExplorer.repaint();  
        canvas.drawImage(bg,0,0,null); 
        populateInventory();
        for (InventoryObject obj : inventory) {
            Animation animation = Animation.get(obj.getSelectedState().animID());
            BufferedImage frame =  animation.getActualFrame() ;
            frame = ImgUtils. resiz (frame,(int) (30 * zoom), (int) (30 * zoom)); 
            canvas.drawImage(frame, obj.zone.x(), obj.zone.y(), null);
        }
        if(invExplorer!=null&&invExplorer.isVisible())
        	invExplorer.revalidate(); //draw the turning inv object
        if(inHandAnim !=null &&inHand !=null && !invExplorer.isVisible()) { //invobj
        	BufferedImage img =  inHandAnim.getActualFrame() ; 
        	img = ImgUtils. resiz (img,(int) (30 * zoom), (int) (30 * zoom)); 
        	canvas.drawImage(img,inHandX-img.getWidth()/2-menuX,inHandY-img.getHeight()/2 -menuY,null);  
        }
        if(inHandObj!=null) {//groundobj
        	if(inHandBmp==null)  inHandBmp=  Animation.get(inHandObj.getSelectedState().animID()).getLastFrame() ; 
        	   
        	canvas.drawImage(inHandBmp,inHandX-inHandBmp.getWidth()/2,inHandY-inHandBmp.getHeight()/2,null);  
        }else inHandBmp = null;
    } catch (Exception e) { e.printStackTrace(); }}
    
	//############################################################################
	public void populateInventory() {if(inventory.size()==0)return; 
		int x = (int) ((invZone().x()) * zoom) ; int y = (int) ((invZone().y()) * zoom);
        for(InventoryObject obj : inventory) {
        	obj.setZone(new Zone(x,y,(int) (x+30 * zoom),(int) (y+30 * zoom),ZoneType.RECTANGLE ));
        	x+=30 * zoom;
        	if(x>(invZone().x()+invZone().w()-20) * zoom) {
        		x=(int) (invZone().x() * zoom);
        		y+=30 * zoom;
        	}
        }
	}
	//############################################################################methods
	//############################################################################
	public void addInvObject(InventoryObject obj) { Main.log("adding "+obj.title()+" in inventory bar");
		inventory.add(obj); 
		if(obj.selectedState==null)
			Main.instance.addAsh(new Ashe(obj.initialState().line.getId(),AshType.IS_IN_STATE,1));
	}
	//############################################################################
	public void removeItem(BasicObject basicObject) { Utils.log("removing  "+basicObject.title()+" from inventory bar");
		inventory.remove(basicObject); 
		if(inHand != null)
		Main.instance.removeAsh(new Ashe(inHand.line.getId(),AshType.HAS_IN_HAND,1));
		inHand=null; inHandAnim=null; 
	}//############################################################################
	public void setZoom(float zoom) { try {
		 this.zoom=zoom;  
		 menuX = (int) (menuZone().x()*Main.zoom);  menuY = (int) (menuZone().y()*Main.zoom); 
			menuWidth = (int) (menuZone().w()*Main.zoom);  menuHeight = (int) (menuZone().h()*Main.zoom);    
			setBounds(menuX ,menuY,menuWidth,menuHeight);
			Utils.setSize(this, menuWidth, menuHeight);
			 bg=ImgUtils.getPicture(bgFolder()); 
			bg = ImgUtils.scal (bg, zoom);
	} catch (Exception e) { e.printStackTrace(); }}
	
	//############################################################################
	//############################################################################events
	public boolean invObjectHasActed(Point clickPoint) { //sent by maingame
		if(inHand==null)
			return false;
		Set<Action> actions = Action.getByDetector(inHand.selectedState );
		for(Action action:actions) {  
			if(Main.instance.actionMgr.canDoAction(action,Main.instance.player)) {  
				Main.instance.actionMgr.execute(action,Main.instance.player);
				Main.instance.removeAsh(new Ashe(inHand.line.getId(),AshType.HAS_IN_HAND,1));
				inHand=null; inHandAnim=null; 
				return true;
			}
		
		}
		return false;
	}
	//############################################################################
	public boolean invObjectHasActedOnCharacter(Point p) { return false; } //useless for the moment
	
	//###################################################  
	private boolean handleClicInventoryBar(int x, int y) {  try { if(Main.instance.actionMgr.isfreezed())return false;
		for(InventoryObject obj : inventory) 
			if(obj.zone.isInZone( new Point((int) (x*zoom ),(int) (y*zoom ))))  { 
				if(inHand == null && inHandObj == null) {  Utils.log("clicked on "+obj.title());
					Main.instance.addAsh(new Ashe(obj.line.getId(),AshType.HAS_IN_HAND,1));
					inHand=obj;
					inHandAnim=Animation.get(inHand.getSelectedState().animID());  
					return true;
				}
				if(inHand != null) {  Utils.log("clicked on "+obj.title()+" with "+inHand.title());
				
					if(inHand.line.getId() == obj.line.getId()) {
						Main.instance.removeAsh(new Ashe(inHand.line.getId(),AshType.HAS_IN_HAND,1));
						inHand=null;inHandAnim = null;
						return false;
					}
					
					InventoryObject oldInHand = inHand;
					Main.instance.addAsh(new Ashe(obj.line.getId(),AshType.IS_CLICKED_ON,1));
					Set<Action> actions = Action.getByDetector(obj.selectedState );
					for(Action action:actions)    
						if(Main.instance.actionMgr.canDoAction(action,Main.instance.player))   
							Main.instance.actionMgr.execute(action,Main.instance.player);
  
					Main.instance.removeAsh(new Ashe(obj.line.getId(),AshType.IS_CLICKED_ON,1));
					Main.instance.removeAsh(new Ashe(obj.line.getId(),AshType.HAS_IN_HAND,1));
					Main.instance.removeAsh(new Ashe(oldInHand.line.getId(),AshType.HAS_IN_HAND,1));
				 
				} 
			} 
		
		if(inHand != null) {  Utils.log("invzone clicked with "+inHand.title()); 
			Main.instance.removeAsh(new Ashe(inHand.line.getId(),AshType.HAS_IN_HAND,1));
			inHand=null;  
			inHandAnim=null;
			return false;
		}
	} catch (Exception e) { e.printStackTrace(); }
	return false;  }
	  
	//###############################################################################################
	//############################################################################################### clic
	public boolean clic(int x, int y) {try {  
		 Point p = new Point( (int) ((x-menuX)/Main.zoom ),(int) ((y-menuY)/Main.zoom )) ; 
				 
				if (inHand != null && dragObjZone().isInZone( p) ) {
					invExplorer.setVisible(true); 
					freezed=true;   
				}
				else if (igMenuOpenerZone().isInZone(  p)) {
					igMenu.goVisibility(true); 
					freezed=true;  	
				} else if (invZone().isInZone( p ))  
				 handleClicInventoryBar(p.x,p.y);
			  
				return true;
		} catch (Exception e) { e.printStackTrace(); } return false;     
	}
	//###############################################################################################
	public boolean hasObject(int x, int y) { 
		 Point p = new Point( (int) (x-menuZone().x() ),(int) (y-menuZone().y() )) ;
		 if (invZone().isInZone( p ))  
				if(handleClicInventoryBar(x, y))return true;
		return false;
	}
	
	//###############################################################################################SQL values
	public String bgFolder(){return "games/"+Main .gameName+"/"+getGameInfo().getString("menubar", "bgname") ;}
	public Zone menuZone() { return Zone.get(getGameInfo().getInt("menubar","bgzone"));}
	public Zone invZone() { return Zone.get(getGameInfo().getInt("menubar","invzone"));}
	public Zone dragObjZone() { return Zone.get(getGameInfo().getInt("menubar","dragobjzone"));} 
	public Zone igMenuOpenerZone() { return Zone.get(getGameInfo().getInt("menubar","igmenuopenerzone"));} 
	public static SQLLine getGameInfo() { return SQLTable.getTable("gameinfo").getLines().get(0); }
}
