package kq7;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Map.Entry;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import agame.Ashe; 
import agame.Ashe.AshType;
import agame.GameCharacter;
import agame.GameCharacterStairManager;
import agame.MainGameActionManager;
import agame.MainGameObjectsManager;
import agame.MainGameTpManager;
import agame.NPGManager;
import agame.SoundPlayer;
import agame.Utils;
import agame.Utils.Pixel;
import agame.Utils.Point;
import agame.Utils.Position;
import gameobjects.Action;
import gameobjects.BasicObject;
import gameobjects.Chapter;
import gameobjects.Character.PosType;
import gameobjects.CharacterState;
import gameobjects.GameObject;
import gameobjects.InventoryObject;
import gameobjects.ObjectState;
import gameobjects.Scene;
import gameobjects.SpawnPoint;
import gameobjects.Zone;
 
	public class Main extends JPanel implements Runnable { private static final long serialVersionUID = 1L;
		 
		//managers
		public MainGameActionManager actionMgr; public MainGameObjectsManager objMgr; public MainGameTpManager tpMgr; public NPGManager npgMgr;  
		  
		//video, panels, thread
		 JScrollPane scrollGame; public VideoPanel videoPanel; Thread updateThread; public static Main instance; 
		
		//game engine stuff
		public static Connection sql = null; boolean continuer = false; public static String gameName ="game-name"; public GameCharacter player;
		
		//game stuff 
		public static float zoom=1.0f; public List<Ashe>currentAshes = new CopyOnWriteArrayList<>(); 
		public SoundPlayer soundPlayer = new SoundPlayer();
		
		//Scene stuff
		public Scene actualSceneOnScreen; public List<Pixel> priorityArray; public BufferedImage backgroundImage; 
		 Ashe sceneExited; 
		 
		//save game stuff
		List<Ashe>savedAshes = new CopyOnWriteArrayList<>();   
		Scene savedScenePosition; Point savedPlayerPosition;  CharacterState savedPlayerState;
 
		private Chapter actualChapter;  
		 
		
		long lastRun = System.currentTimeMillis();
		//##################################################################the thread:
		@Override
		public void run() { 
		    try {
		        while (continuer) {long betweenRuns = System.currentTimeMillis()-lastRun;
		            long start = System.currentTimeMillis();
		           
		             
		            SwingUtilities.invokeAndWait(() -> {
		                try { 
		                    actionMgr.run();
		                    npgMgr.run();
		                    if (player != null)
		                        player.run();
		                    videoPanel.scrollGame.repaint(); 
		                    if (player != null && player.feetPosition != null)
		                        videoPanel.centerScrollPaneOne(player.feetX(), player.feetY());
		                } catch (Exception e) {
		                    e.printStackTrace();
		                }
		                
		            });
		            long elapsedTime = System.currentTimeMillis() - start;
		            long timeToSleep = Math.max(0, 100-(elapsedTime + betweenRuns));
		            Thread.sleep(timeToSleep); 

		            lastRun = System.currentTimeMillis();
		        }
		    } catch (InterruptedException | InvocationTargetException e) {
		        e.printStackTrace();
		    }
		}
		//##################################################################
		public Main ( ) {  
			instance=this;
			try { //open sql and set the logger to the file
				//new File("output.txt").delete();
				//PrintStream out = new PrintStream( new FileOutputStream("output.txt", true), false);
				//System.setOut(out); System.setErr(out); 
				sql = DriverManager.getConnection("jdbc:sqlite::resource:game.npcma" );
			} catch ( Exception e) { }
			
			 videoPanel = new VideoPanel(); //create the main panel
		}
		 
		public void init() { 
			Iterator<Chapter> iterator =  Chapter.getAll() .iterator();
			 while(iterator.hasNext()) {
				 Chapter ch = iterator.next();
				 if(ch.number() == 0)
					 actualChapter=ch;
			 }
			actionMgr = new MainGameActionManager(this);
			objMgr= new MainGameObjectsManager(this);
			tpMgr = new MainGameTpManager(this);
			npgMgr=new NPGManager(this); 
			
			changeScene(actualChapter.startScene() ); 
			videoPanel.centerScrollPaneOne(actualChapter.positionStart().x,actualChapter.positionStart().y);
	 
			actionMgr.execute(actualChapter.firstAction(), null);
			continuer=true;
			updateThread = new Thread(this);
			updateThread.start();
		}
		//###############################################
		public void changeScene(Scene destScene ) {  
			 if(player!=null&&player.path!=null) {player.path.clear();player.path=null;}

			if(sceneExited!=null) this.removeAsh(sceneExited);
			if(actualSceneOnScreen!=null) {
				sceneExited=new Ashe(actualSceneOnScreen.line.getId(),AshType.SCENE_WAS_EXITED,1);
				if(sceneExited!=null)this.addAsh(sceneExited);
			}
				 
			changeScene(destScene,true );
			 ImgUtils.mappe.clear();
			if(player != null) {
				if(sceneExited!=null)//do exit actions if needed
					actionMgr.handleAction( Action.getByDetector(sceneExited.possessor() ),player); 
				
				//do enter actions if needed
				Ashe ashSceneEntered = new Ashe(actualSceneOnScreen.line.getId(),AshType.SCENE_HAS_BEEN_ENTERED,1);
				this.addAsh(ashSceneEntered);
				actionMgr.handleAction( Action.getByDetector(actualSceneOnScreen ),player);   
				this.removeAsh(ashSceneEntered);
				
				//record the visit
				Ashe ashSceneVisited = new Ashe(actualSceneOnScreen.line.getId(),AshType.SCENE_HAS_BEEN_VISITED,1);
				if(!this.haveAsh(ashSceneVisited))
					this.addAsh(ashSceneVisited);
				player.stairManager = new GameCharacterStairManager(this,player); 
			}
			 
		}
		//###############################################
		public void changeScene(Scene destScene ,boolean dontDoAshJob) {  
			    actualSceneOnScreen = destScene; 
				if(player!=null)player.actualScene = destScene;
				backgroundImage = actualSceneOnScreen.bgImage() ;
				backgroundImage = ImgUtils.scal(backgroundImage, zoom);
				Utils.setSize(videoPanel,backgroundImage.getWidth(), backgroundImage.getHeight() );
				new Thread(new Runnable() { @Override public void run() {try {  //go faster in the scene 
	 	              priorityArray = Utils.getMatrix(actualSceneOnScreen, false);
				} catch (Exception e) { 
					priorityArray = null;
					log("no priority bg found for " + destScene.title());
				}
				} }).start();
 			
			objMgr.handleSceneChanged( ); //draw the new ground obj please 
			soundPlayer.stopLoop();
			if(destScene.music().contains(".mid"))
				soundPlayer.playMidi(destScene.getFolder()+destScene.music(),actualSceneOnScreen.replaceSong(),actualSceneOnScreen.loopMusic()!=-998);//and play nice song
			else {
				if(  destScene.replaceSong()) {
					if(soundPlayer.isPlayingMidi())soundPlayer.stopMidi(); 
					soundPlayer.playAudio(destScene.getFolder()+destScene.music(), true,destScene.loopMusic());
				}
			}			
			 
			if(player!=null)player.stairManager = new GameCharacterStairManager(this,player); 
		}
		//##################################################################
		public void handleCharacterArrived(GameCharacter c) {
			SwingUtilities.invokeLater(() -> {  
				boolean wasFreeze = actionMgr.freezedWalk;
				if(c.equals(player))
				actionMgr.freezedWalk=false;
				tpMgr.handleCharacterArrived(c); 
				objMgr.handleCharacterArrived(c);
				npgMgr.handleCharacterArrived(c);
				if(!c.equals(player)||wasFreeze)for(Zone a:SpawnPoint.getByOwner(c.actualScene.line.getId()))
					if(Utils.getDistanceBetweenPoints(c.feetPosition, a.mid())<20) {
						addAsh(new Ashe(a.line.getId(),AshType.HAS_BEEN_COLLIDED,1));
						actionMgr.handleAction(Action.getByDetector(a),c);
						removeAsh(new Ashe(a.line.getId(),AshType.HAS_BEEN_COLLIDED,1));
					}
			});
		}
		
		//##################################################################
 
		//###############################################
		public void removeAsh(Ashe ash) { 
			int contain = 0; int index=-1; int i = 0;
			for(Ashe a : currentAshes){ 
				if(a!=null&&a.equals(ash)){
					contain++;
					index = i; 
				}
				i++;
			}
			if(index!=-1 && contain !=1) 
				currentAshes.remove(index);   
			else 
				currentAshes.remove(ash);  
		} 
		
		public void addAsh(Ashe ash) {   currentAshes.add(ash);  }  
		public int howManyAsh(Ashe ash){
			  int i = 0;
			  for(Ashe a : currentAshes) 
					if(a!=null&&a.equals(ash)) 
						i++; 
			  return i;
		} 
		 
		public boolean haveAsh(Ashe ash) {  return currentAshes.contains(ash); }
		//############################################### 
		//need to be rewritten if future 
		//TODO :
		public void saveBeforeDead(){Utils.log("save"+currentAshes);
			savedAshes = new CopyOnWriteArrayList<>( currentAshes);
			savedAshes.add(new Ashe(player.id,AshType.WILL_BE_SPAWNED,1));
			savedScenePosition = this.actualSceneOnScreen;
			savedPlayerPosition = player.feetPosition; 
			savedPlayerState=player.getSelectedStat(); 
			saveToFile();
		}
		public void restoreBeforeDead(){actionMgr.freezedWalk=false;videoPanel.menuBar.inHand=null;this.npgMgr.allThePnj.clear();player=null;
			videoPanel.menuBar.inventory.clear();GameObject.reset();
			currentAshes = new CopyOnWriteArrayList<>(savedAshes);
			Utils.log("restore"+currentAshes);
			for(Ashe a : currentAshes){if(a.type() == AshType.HAS_IN_HAND||a.type() == AshType.SCENE_HAS_BEEN_ENTERED||a.type() == AshType.IS_CLICKED_ON) {
				currentAshes.remove(a);
				continue;
			}
				if(a.type() == AshType.HAS_IN_INVENTORY)
					videoPanel.menuBar.inventory.add((InventoryObject) a.possessor());
				if(a.type()==AshType.IS_IN_STATE ) { 
					if(a.possessor().possessorObj() instanceof BasicObject)
					((BasicObject)a.possessor().possessorObj()).setSelectedState((ObjectState) a.possessor());
					else currentAshes.remove(a);
				}
				if(a.type() == AshType.WILL_BE_SPAWNED) {
					npgMgr.spawnPnj(a);
					currentAshes.remove(a);
		}}
			this.changeScene(savedScenePosition,true);
			player.setPos(savedPlayerPosition); 
			player.setSelectedStat(savedPlayerState);
			videoPanel.centerScrollPaneOne(player.feetX(), player.feetY());
		}
	 
		public void saveToFile() {
		    try { Properties props = new Properties(); int i = 0;
		    props.setProperty(new Ashe(player.id,AshType.WILL_BE_SPAWNED,1).toSqlValue()+":"+i, new Ashe(player.id,AshType.WILL_BE_SPAWNED,1).toSqlValue());i++;
		    	for(Ashe ash : currentAshes){
		    		props.setProperty(ash.toSqlValue()+":"+i, ash.toSqlValue());i++;
		    	} 
		    	 
		    	 
		    	props.setProperty("scene", savedScenePosition.line.getId()+"");
		    	props.setProperty("pos", savedPlayerPosition.x +","+savedPlayerPosition.y);
		    	props.setProperty("state", savedPlayerState.line.getId()+"");
		    	
		    	for(Entry<Action, Long> a : actionMgr.currentTimers.entrySet())
		    		props.setProperty(new Ashe(a.getKey().line.getId(),AshType.ACTION_TO_DO_WITH_BEFORE_CONDITION_CHECK,0).toSqlValue(), a.getValue()+"");
		        File f = new File("save");
		        OutputStream out = new FileOutputStream( f ); 
		        props.store(out, "User properties");
		        out.close();
		    }
		    catch (Exception e ) {
		        e.printStackTrace();
		    }
		}
		public void restoreGame() {try {if(!new File("save").exists())return;
			actionMgr.pendingAshesToSet.clear();currentAshes.clear(); 
			FileInputStream fs = new FileInputStream( new File("save") );
			videoPanel.menuBar.inHand=null;
			videoPanel.menuBar.inventory.clear();
			savedAshes = new CopyOnWriteArrayList<>();
			  Properties props = new Properties(); props.load(fs); 
		    	for( Entry<Object, Object> e : props.entrySet()){
		    		String key = (String)e.getKey();
		    		if(key.equals("scene"))savedScenePosition = Scene.get(Integer.parseInt((String) e.getValue())); 
		    		else if(key.equals("state"))savedPlayerState = CharacterState.get(Integer.parseInt((String) e.getValue())); 
		    		else if(key.equals("pos"))savedPlayerPosition = new Position(Integer.parseInt(((String) e.getValue()).split(",")[0]),Integer.parseInt(((String) e.getValue()).split(",")[1]),PosType.BOTTOM.getValeur()); 
		    		else {
		    			Ashe ash = Ashe.fromSqlValue(key);
		    			if(ash.type()==AshType.ACTION_TO_DO_WITH_BEFORE_CONDITION_CHECK)
		    				actionMgr.currentTimers.put( (Action) ash.possessor() , Long.parseLong((String)e.getValue()));
		    			else savedAshes.add(ash);
		    			
		    		}
		    	} 
		    	 fs.close();
		    	 restoreBeforeDead() ;
		    }
		    catch (Exception e ) {
		        e.printStackTrace();
		    }
		}
		public static void log(String string) {
			System.out.println(string);
		}
		  
	}

	 
		 	
