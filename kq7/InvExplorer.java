package kq7;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;
import javax.swing.JTextField;
 

import agame.Ashe;
import agame.Utils;
import agame.Ashe.AshType;
import agame.Utils.Point; 
import gameobjects.Action;
import gameobjects.Animation;
import gameobjects.Message;
import gameobjects.SpecialZoneInv;
import gameobjects.Zone; 

public class InvExplorer extends JPanel  {//TODO:need to center this
	 
	 int movableX=-1,movableY;
private BufferedImage bg;   JTextField text = new JTextField();
	//######################################
	 
    public InvExplorer(  ) {try {  setLayout(null);
        MenuBar.invExplorer = this;
        bg =  ImgUtils.getPicture(invExplorerBg() );
        Utils.setSize(this, bg.getWidth(), bg.getHeight());
    	setBounds(0,0, bg.getWidth(), bg.getHeight());
    	setLayout(null);
        setVisible(false); 
 		
 		text.setBounds(invZoneTitles().x(),invZoneTitles().y(),invZoneTitles().w(),invZoneTitles().h());
 		text.setBackground(Color.BLACK);
 		text.setForeground(Color.WHITE); 
        add(text);
    } catch (Exception e) {e.printStackTrace(); } }
 
  //######################################
    @Override protected void paintComponent(Graphics canvas) {try {
        super.paintComponent(canvas);
        String txt = Main.instance.videoPanel.menuBar.inHand.title();
        if(Main.instance.videoPanel.menuBar.inHand.selectedState.translateId()!=-998)
        	txt = Message.get(Main.instance.videoPanel.menuBar.inHand.selectedState.translateId()).line.getString("text");
       text.setText(txt);
        canvas.drawImage(bg, 0,0,null); 
        
        BufferedImage button =  ImgUtils.getPicture(exitButtonInvExplorerBg()) ;
        button = ImgUtils.scal (button, Main.zoom);
        canvas.drawImage(button, (int)(closeInvExplorerZone().x()*Main.zoom),(int)(closeInvExplorerZone().y()*Main.zoom),null); 
        
        if(Main.instance.videoPanel.menuBar.inHand != null&&Main.instance.videoPanel.menuBar.inHandAnim != null) {
        	BufferedImage img =  Main.instance.videoPanel.menuBar.inHandAnim.getActualFrame() ;
        	img = ImgUtils.scal (img, Main.zoom);
        	int x=bg.getWidth()/2-img.getWidth()/2;
			int y= bg.getHeight()/2-img.getHeight()/2;
        	canvas.drawImage(img, x,y,null);
        	 
            
        }
    } catch (Exception e) {e.printStackTrace(); }}
     
	//######################################
	private void moveObjectInExplorer(int newX, int newY) { 
 	   if( Math.abs(movableX - newX) <10 && Math.abs(movableY - newY) <10 )return; 
 	    if (newX > movableX && newY > movableY)  Main.instance.videoPanel.menuBar.inHandAnim.rewindOneFrame();  
 	    else if (newX < movableX && newY > movableY) Main.instance.videoPanel.menuBar.inHandAnim.forwardOneFrame();  
 	    else if (newX > movableX && newY < movableY)  Main.instance.videoPanel.menuBar.inHandAnim.rewindOneFrame();  
 	    else if (newX < movableX && newY < movableY) Main.instance.videoPanel.menuBar.inHandAnim.forwardOneFrame();  
 	    movableX = newX;
 	    movableY = newY;
	}
	 
	//######################################
	private void closInvExplorerIfNeeded(int x, int y) {
			if(closeInvExplorerZone().isInZone(new Point(x,y))) {
				Main.instance.videoPanel.menuBar.freezed=false;
				setVisible(false); 
				Main.instance.removeAsh(new Ashe(Main.instance.videoPanel.menuBar.inHand.line.getId(),AshType.HAS_IN_HAND,1));

				Main.instance.videoPanel.menuBar.inHandAnim=null;
				Main.instance.videoPanel.menuBar.inHand =null;
	        } 
	} 
	//###################################################  
	private void handleZoneClickInvExplorer(int zoomedX, int zoomedY) {try {
			 if(Main.instance.videoPanel.menuBar.inHand != null && Main.instance.videoPanel.menuBar.inHandAnim != null)
				for(Zone z: SpecialZoneInv.getByOwner(Main.instance.videoPanel.menuBar.inHand.getSelectedState() .line.getId())) {//tcheck inv zones
					
					//bg = ImgUtils.getPicture(invExplorerBg()); 
					BufferedImage handImage = Main.instance.videoPanel.menuBar.inHandAnim.getActualFrame(); 
					int objectWidth = handImage.getWidth() ;
					int objectHeight = handImage.getHeight() ; 
					int imageX = (bg.getWidth() - objectWidth) / 2;
					int imageY = (bg.getHeight() - objectHeight) / 2; 
					 int relativeX = zoomedX - imageX;
		                int relativeY = zoomedY - imageY;

		                // Adjust relative coordinates to unzoomed coordinates
		                int unzoomedRelativeX = (int) (relativeX / Main.zoom);
		                int unzoomedRelativeY = (int) (relativeY / Main.zoom);

		                Point p = new Point(unzoomedRelativeX, unzoomedRelativeY);
		                Main.log(unzoomedRelativeX+" "+unzoomedRelativeY);
					 if(z.isInZone(p) 
							 && (((SpecialZoneInv)z).frames.contains(-1) || ((SpecialZoneInv)z).frames.contains(Main.instance.videoPanel.menuBar.inHandAnim.currentIndex()))) {
						Main.instance.actionMgr.handleAction( Action.getByDetector(z ),Main.instance.player);
						Main.instance.videoPanel.menuBar.inHandAnim=Animation.get(Main.instance.videoPanel.menuBar.inHand.getSelectedState().animID());//if changed state
						Main.instance.videoPanel.menuBar.inHandAnim.forwardAll(); 
					}  
				 }  
	} catch (Exception e) { e.printStackTrace(); }  }
	
	public Zone invZoneTitles() { return Zone.get(MenuBar.getGameInfo().getInt("menubar","invzonetitles"));}
	public String invExplorerBg(){return "games/"+Main .gameName+"/"+MenuBar.getGameInfo().getString("menubar", "invbgname") ;}
	public String exitButtonInvExplorerBg(){return "games/"+Main .gameName+"/"+MenuBar.getGameInfo().getString("menubar", "exitexplorerbg") ;}
	public Zone closeInvExplorerZone() { return Zone.get(MenuBar.getGameInfo().getInt("menubar","closeinvexplorerzone"));}
	public void setZoom(float z) {
		 bg =  ImgUtils.getPicture(invExplorerBg() );
	        bg = ImgUtils.scal (bg, Main.zoom);
		Utils.setSize(this, bg.getWidth(), bg.getHeight());
		text.setBounds((int)(invZoneTitles().x()*Main.zoom),(int)(invZoneTitles().y()*Main.zoom),(int)(invZoneTitles().w()*Main.zoom),(int)(invZoneTitles().h()*Main.zoom));
	}
 
 
	 public void mouseMoved(int x, int y) {
		moveObjectInExplorer(x, y);
	}

	public void mouseReleased(int xx, int yy) {
		if(!isVisible())return ;    
     	int x = (int) ((int) xx/Main.zoom); int y = (int) ((int) yy/Main.zoom); 
     	closInvExplorerIfNeeded(x, y);
     	handleZoneClickInvExplorer(xx, yy); 
	}
	
}