package kq7;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeListener;

import agame.Ashe;
import agame.Ashe.AshType;

//import com.xuggle.mediatool.IMediaReader;
//import com.xuggle.mediatool.MediaListenerAdapter;
//import com.xuggle.mediatool.ToolFactory;
//import com.xuggle.mediatool.event.IVideoPictureEvent;

import agame.SoundPlayer;
import agame.Utils;
import agame.Utils.Point;
import gameobjects.Action;
import gameobjects.Animation;
import gameobjects.SpawnPoint;
import gameobjects.Zone;
import javazoom.jl.player.advanced.AdvancedPlayer; 
 

public class VideoPanel extends JPanel implements MouseListener, MouseMotionListener  {  
	    
    JScrollPane scrollGame; 
	public static MenuBar menuBar; 
	public static JFrame frame;  
	JPanel game;
	private int inHandX;
	private int inHandY;
	
	//###############################################
	public VideoPanel( ) {try { 

		frame = new JFrame("king quest 7 v0.1");
				 
	 
		frame. addComponentListener(new ComponentAdapter() { @Override public void componentResized(ComponentEvent e) { 
	        scrollGame.setBounds(0, 0, frame .getSize().width, frame .getSize().height);
	        calculateAndSetInitialZoom(); 
	        }  });//change zoom on resize
				
		frame.addWindowListener(new WindowAdapter() { //when close program
			@Override public void windowClosing(WindowEvent e) {try {  
				Main.instance.saveBeforeDead(); 
				Main.instance.soundPlayer.closeAll(); 
				Main.instance.continuer = false;  
				Main.instance.sql.close();
				frame.dispose();
			} catch (SQLException e1) { e1.printStackTrace(); } System.exit(0); }}); 
		 
		game = new JPanel();
		game.setLayout(null); 
		menuBar = new MenuBar(); 
		scrollGame = new JScrollPane(this);
		scrollGame.setOpaque(false);
		scrollGame.addMouseListener(this);
		scrollGame.addMouseMotionListener(this);
		scrollGame.setBounds(0, 0, 640, 480);  
		game.add(menuBar);
		game.add(MenuBar.igMenu);game.add(MenuBar.invExplorer);
		game.add(scrollGame);
		menuBar.setOpaque(true); 
		frame.getContentPane().add(game);
		frame.pack();
		frame.setVisible(true);
		frame.setSize(640, 480);
		Main.instance.videoPanel=this;
		Main.instance.init();//playIntro();
         
    } catch (Exception e) { e.printStackTrace(); }}

	//###############################################
	 
	 void painte(Graphics g) { JViewport viewport = scrollGame.getViewport();
    	g.drawImage(Main.instance.backgroundImage, 0,0,this);    //draw background
		if(!menuBar.freezed  ) {
			 
			Main.instance.objMgr.drawObjectsBackground(g);//if fullscreen obj show this+childs
			if(!Main.instance.objMgr.isFullScreen()){//or draw the obje we forced to zindex<50  
				Main.instance.npgMgr.paintImage(g);//then, paint player, characters, and object with own zindex
				Main.instance.objMgr.drawObjectsFront(g);//then draw the obje we forced to zindex>50
			}
    		 
		}
		 
		if(menuBar.inHandAnim !=null &&menuBar.inHand !=null && !menuBar.invExplorer.isVisible()) { //invobj
        	BufferedImage img =  menuBar.inHandAnim.getActualFrame() ;
        	img = ImgUtils. resiz (img,(int) (30 * Main.zoom), (int) (30 * Main.zoom)); 
        	g.drawImage(img,inHandX-img.getWidth()/2 ,inHandY-img.getHeight()/2 ,null); 
        }
        if(menuBar.inHandObj!=null) {//groundobj
        	 
             	if(menuBar.inHandBmp==null) {menuBar.inHandBmp=  Animation.get(menuBar.inHandObj.getSelectedState().animID()).getLastFrame() ; 
             	menuBar.inHandBmp = ImgUtils. resiz (menuBar.inHandBmp,(int) (30 * Main.zoom), (int) (30 * Main.zoom)); }
             	g.drawImage(menuBar.inHandBmp,inHandX-menuBar.inHandBmp.getWidth()/2,inHandY-menuBar.inHandBmp.getHeight()/2,null);  
              
        }else menuBar.inHandBmp = null;
		
    }
    @Override protected void paintComponent(Graphics g) {
       painte(g);
    }
  //###############################################
    public static void calculateAndSetInitialZoom() {
        int screenHeight = frame.getHeight();
        int initialImageHeight = Main.instance.actualSceneOnScreen.bgImage().getHeight(); 
        float initialZoom = (float) screenHeight / initialImageHeight;
        setZoom(initialZoom);
    }

  //###############################################
	public static void setZoom(float z) {
		 Main.zoom = z;
		 MenuBar.igMenu.setZoom(z);
		 MenuBar.invExplorer.setZoom(z);
         menuBar.setZoom(z);
         
		 Main.instance.backgroundImage=ImgUtils.scal(Main.instance.actualSceneOnScreen.bgImage(),z);
			 
	           Utils.setSize(Main.instance.videoPanel,  Main.instance.backgroundImage.getWidth(), Main.instance.backgroundImage.getHeight());
	             
	}
	
	//###############################################
			public void centerScrollPaneOne(int characterX, int characterY) {  
			    JViewport viewport = scrollGame.getViewport();
			    characterX*=Main.zoom;characterY*=Main.zoom;
			    int viewX = Math.max(0, characterX - viewport.getWidth() / 2);
			    int viewY = Math.max(0, characterY - viewport.getHeight() / 2); 
			    int sceneWidth = Main.instance.backgroundImage.getWidth();
			    int sceneHeight = Main.instance.backgroundImage.getHeight(); 
			    if (characterX + viewport.getWidth() / 2 > sceneWidth)  viewX = sceneWidth - viewport.getWidth();  
			    if (characterY + viewport.getHeight() / 2 > sceneHeight) viewY = sceneHeight - viewport.getHeight();  
			    viewport.setViewPosition(new java.awt.Point(viewX, viewY));  
			     
			}


	
	  //###############################################
	@Override public void mouseMoved(MouseEvent e) {  
		java.awt.Point localPoint = SwingUtilities.convertPoint(e.getComponent(), e.getPoint(), VideoPanel.this);
		if(Main.instance.player!=null)
			Main.instance.tpMgr.changeCursor(new Point((int) (localPoint.x/Main.zoom),(int) (localPoint.y/Main.zoom)));
		 if(menuBar.inHand!=null||menuBar.inHandObj!=null) { 
    		MenuBar.inHandX = e.getX(); 
    		MenuBar.inHandY = e.getY(); 
    		inHandX = localPoint.x; 
    		inHandY = localPoint.y; 
    		 
    	}
		
	}
	 //###############################################
	@Override public void mouseDragged(MouseEvent e) { 
		if( MenuBar.invExplorer.isVisible() && MenuBar.invExplorer.getBounds().contains(e.getPoint())) 
				MenuBar.invExplorer.mouseMoved(e.getX(),e.getY()) ;
		mouseMoved(e); 
	}

	  //###############################################
	@Override public void mousePressed(MouseEvent e) {
		java.awt.Point localPoint = SwingUtilities.convertPoint(e.getComponent(), e.getPoint(), VideoPanel.this);
        
		frame.setCursor( null );
    	 
		
		if( menuBar.getBounds().contains(e.getPoint())) {
			menuBar.clic(e.getPoint().x,e.getPoint().y) ;
			return;
		}
		if( MenuBar.igMenu.isVisible() && MenuBar.igMenu.getBounds().contains(e.getPoint())) {
			MenuBar.igMenu.mouseReleased(e.getX(),e.getY()) ;
			return;
		}
		if( MenuBar.invExplorer.isVisible() && MenuBar.invExplorer.getBounds().contains(e.getPoint())) {
			MenuBar.invExplorer.mouseReleased(e.getX(),e.getY()) ;
			return;
		}
		
		SwingUtilities.invokeLater(() -> { 
			if(Main.instance.actionMgr.actualAudio != null)
				try {
					Main.instance.actionMgr.actualAudio.stop();
				} catch (Exception e1) {
					Utils.log("audio nulled");Main.instance.actionMgr.actualAudio=null;
				}
			if(menuBar.freezed || Main.instance.actionMgr.isfreezed()|| !Main.instance.objMgr.isFullScreen()&&Main.instance.actionMgr.freezedWalk || Main.instance.actionMgr.pendingAshesToSet.size()!=0) { 
				Utils.log("clic canceled by freezer"+Main.instance.actionMgr.freezedWalk); return; }
			Main.instance.actionMgr.freezedWalk=false;//if fullscreen dont go to desired position
			if(!Main.instance.player.stairManager.isBusy())
				if(e.getButton() == 1 ) {   
					Main.instance.player.zoneClicked = null;
					Main.instance.player.characterToRejoin=null;
					Main.instance.player.tpClicked=null; 
					Point p = new Point((int) (localPoint.x/Main.zoom),(int) (localPoint.y/Main.zoom)); 
					if(!Main.instance.objMgr.isFullScreen())if(menuBar.invObjectHasActedOnCharacter(p)) return ; 
						if(menuBar.invObjectHasActed(p)) return ;  
						if(Main.instance.objMgr.objectHasBeenClicked(Main.instance.player,p))return;
						if(Main.instance.tpMgr.tpHasBeenClicked(p,Main.instance.player)) return;  
						if(!Main.instance.objMgr.isFullScreen())if(Main.instance.npgMgr.clickHasActedOnCharacter(p,Main.instance.player)) return ; 
				 
						if(!Main.instance.objMgr.isFullScreen()) { 
							 boolean press = true;
							 Main.instance.addAsh(new Ashe(Main.instance.actualSceneOnScreen.id,AshType.SCENE_HAS_BEEN_CLICKED,1));
							if(Main.instance.actionMgr.handleAction(Action.getByDetector(Main.instance.actualSceneOnScreen), Main.instance.player)) {
								Utils.log("clic cancel by scene press");
							 press=false;
							}
							Main.instance.removeAsh(new Ashe(Main.instance.actualSceneOnScreen.id,AshType.SCENE_HAS_BEEN_CLICKED,1)); 
							 
							for(Zone sp:SpawnPoint.getByOwner(Main.instance.actualSceneOnScreen.id))
								if(sp.isInZone(p)) {
									Main.instance.addAsh(new Ashe(sp.id,AshType.ZONE_HAS_BEEN_CLICKED,1));
									if(Main.instance.actionMgr.handleAction(Action.getByDetector(sp), Main.instance.player)) {
										Utils.log("clic cancel by spawn press : "+sp.title());
			 							 press=false;
									}
									Main.instance.removeAsh(new Ashe(sp.id,AshType.ZONE_HAS_BEEN_CLICKED,1));
								}
						if(press)Main.instance.player.setClicPose(new Point( p.x  , p.y  )); 
						}
					 
					 
						
				} 
				 
		 }); 
		 
		if(e.getButton() == 3) {
			    
               if (Main.instance != null&&! menuBar.freezed&&!Main.instance.objMgr.isFullScreen()) {
                   int x = (int) localPoint.x;
                   int y = (int) localPoint.y;
                   Main.instance.player.setPos(new Point((int) (x / Main.zoom), (int) (y / Main.zoom)));
               }
               return;
		}
		 
         
		
		 
	}

	
	
	 
	
	 
	 
	
	@Override public void mouseReleased(MouseEvent e) { }
	@Override public void mouseClicked(MouseEvent e) { } 
	@Override public void mouseEntered(MouseEvent e) { } 
	@Override public void mouseExited(MouseEvent e) { }
}
 