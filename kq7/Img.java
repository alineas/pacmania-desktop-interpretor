package kq7;

import java.awt.image.BufferedImage;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
  

public class Img {
	
	  String path;  public int shadow; public List<BufferedImage> frames = new ArrayList< >();
	  public Img(String path) {
		  this(path, -1);
	  }
	  
	//##################################################################
	  public Img(String path, int shadow) {
	        this.path = path; 
	        this.shadow = shadow;         
	        try { 
	        	GifDecoder decoder = new GifDecoder(path);
	        	 
	        	for(BufferedImage fram : decoder.getFrames())
	        		frames.add( fram );
	        	 
	        	if (frames.isEmpty()) {
	        		BufferedImage loadedImage = null; 
					InputStream str = Thread.currentThread().getContextClassLoader().getResourceAsStream(path);
					loadedImage = ImageIO.read(str);
				 
				 str.close();
	        		frames.add(loadedImage); 
	        	}
	        	
	        } catch (Exception e) {e.printStackTrace(); }
	       if(shadow !=-1)
	    	   for(BufferedImage fram:frames)
	    		   ImgUtils.applyShadow(fram, shadow);
	    		   
	    }
	//################################################## load in memory
		 
	public int getWidth() { 
		return frames.get(0).getWidth();
	}
	public int getHeight() { 
		return frames.get(0).getHeight();
	}
	

	    
}
