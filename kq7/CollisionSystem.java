package kq7;

import java.util.HashMap;
import java.util.HashSet; 
import java.util.Map;
import java.util.Set;

import agame.Utils.Point;
import gameobjects.Action;
import gameobjects.BasicObject;
import gameobjects.ObjectState;
import gameobjects.Zone;

public class CollisionSystem {

	public final static Set<Point> colli = new HashSet<Point>(); //all the collisions pixels 
	static int tailleGrilleX = 50;  
	static int tailleGrilleY = 50;  
	static Map<Point, Set<Point>> grid = new HashMap<>();  
	static Map<Point, Set<Zone>> zones = new HashMap<>();  
	
	
	public static void enterScene(Set<Zone> set) {try {
		grid.clear(); zones.clear(); colli.clear();  
			for(Zone zone : set) {  
				for (Point point : zone.getPoints()) {
					colli.add(point);  
					Point gridCell = getGridCellByPoint(point);
					Set<Point> pointsWithin = getPointsOfGrid(gridCell);
					if (pointsWithin == null) { pointsWithin = new HashSet<>(); grid.put(gridCell, pointsWithin); }
					pointsWithin.add(point); 
					//if(!Action.isDetector(zone))continue;
					 Set<Zone> zonesWithin = zones.get(gridCell); 
			         if (zonesWithin == null) {
			             zonesWithin = new HashSet<>();
			             zones.put(gridCell, zonesWithin);
			         }
			         if(!zonesWithin.contains(zone))
			        	 zonesWithin.add(zone);
				}  
			}
	} catch (Exception e) {e.printStackTrace();} }  
	
	private static Set<Point> getPointsOfGrid(Point gridCell){ return grid.get(gridCell); }
	public static Set<Zone> getZonesOfGridByPoint(Point gridCell){
		Set<Zone> xxx = zones.get(getGridCellByPoint(gridCell));
		if(xxx==null)xxx=new HashSet();
		return xxx; 
		}
	
	private static Point getGridCellByPoint(Point point) {
	    int celluleGrilleX = point.x / tailleGrilleX;
	    int celluleGrilleY = point.y / tailleGrilleY;
	    return new Point(celluleGrilleX, celluleGrilleY);
	}

	public static boolean isCollided(Point point) {
		
		Set<Point> area = getGridsInArea(point, 200);
        for (Point gridPoint : area)  {
        Set<Point> pointsOfThisCell = getPointsOfGrid(gridPoint);
        if (pointsOfThisCell == null) return false; 
        if( pointsOfThisCell.contains(point))return true;
        }
        return false;
    }
	
	private static Set<Point> getGridsInArea(Point position, int tolerance) {
	    Set<Point> gridsInArea = new HashSet<>(); 
	    
	    for (int i = -tolerance; i <= tolerance; i+=30) {
	        for (int j = -tolerance; j <= tolerance; j+=30) {
	            int x = position.x + i;
	            int y = position.y + j;
	            Point p = new Point(x, y);
	            Point grid = getGridCellByPoint(p); 
	            if(!gridsInArea.contains(grid))gridsInArea.add(grid);
	        }
	    } 
	    return gridsInArea;
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	static Map<Point, Set<Zone>> zonesToClick = new HashMap<>();
	 
	public static Set<Zone> getZonesToClickOfGridByPoint(Point gridCell){ return zonesToClick.get(getGridCellByPoint(gridCell)); }
 
	
	public static void populateZonesToClick(Set<BasicObject> allTheObjects) {
		 zonesToClick.clear(); 
		 
		 for(BasicObject obj : allTheObjects) {
		 
			 for(ObjectState state : ObjectState.getByObject(obj.line.getId()))
				 for(Zone zone : Zone.getByOwner(state.line.getId()))
					 for(Point p : zone.getPoints()) { 
						 Point gridCell = getGridCellByPoint(p); 
							 Set<Zone> zonesWithin = zonesToClick.get(gridCell); 
					         if (zonesWithin == null) {
					             zonesWithin = new HashSet<>();
					             zonesToClick.put(gridCell, zonesWithin);
					         }
					         if(!zonesWithin.contains(zone))
					        	 zonesWithin.add(zone);
					 
			 } 
						 
					 
		 
		 
		 
		 }
		 
		 
		 
		 
		
	}
	
	
}
